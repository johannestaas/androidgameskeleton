package com.fusion.ballinwight.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fusion.ballinwight.BallinWightGame;

public class SplashScreen implements Screen {

	BallinWightGame game;
	SpriteBatch splashBatch;
	Sprite bgSprite;
	Texture bg;
	int width, height;

	public SplashScreen(BallinWightGame g) {
		game = g;
		width = 800;
		height = 480;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		splashBatch.begin();
		splashBatch.draw(bgSprite, 0, 0, width, height);
		splashBatch.end();

		//if (Gdx.input.justTouched())
		//	myGame.setScreen(new GameScreen());

	}

	@Override
	public void resize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public void show() {
		splashBatch = new SpriteBatch();
        bg = new Texture(Gdx.files.internal("title.gif"));
        bgSprite = new Sprite(bg, 0, 0, 800, 480);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
