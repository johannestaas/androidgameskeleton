package com.fusion.ballinwight.models.units;

public abstract class Army {
	protected int strength;
	protected int agility;
	protected int intelligence;
	protected int range;
	protected int hp, xhp;
	protected int mp, xmp;

	public Army() {

	}

	public int attack(Army enemy) {
		if (enemy.dodge(accuracy())) {
			return 0;
		}
		int damage = Math.min(this.damage() - enemy.defend(this), 1);
		return damage;
	}

	public int defend(Army enemy) {
		return Math.min((agility / 3) + (strength / 3), 1);
	}
	
	public int defend(Hero hero) {
		return 0;
	}
	
	protected int damage() {
		int damage = strength;
		return damage;
	}

	protected int accuracy() {
		int accuracy = agility;
		return accuracy;
	}

	public boolean dodge(int enemyAccuracy) {
		int dodge = Math.max(Math.min(1, agility - enemyAccuracy), 10);
		return false;
	}



}
