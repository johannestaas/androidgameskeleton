package com.fusion.ballinwight.models.units;

import com.fusion.ballinwight.models.units.Army;

public class GoblinArmy extends Army {

	public GoblinArmy() {
		super();
		strength = 4;
		intelligence = 1;
		agility = 3;
		range = 1;
	}

}
