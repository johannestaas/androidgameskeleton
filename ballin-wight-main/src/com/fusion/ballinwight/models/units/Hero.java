package com.fusion.ballinwight.models.units;


public abstract class Hero {
	protected int strength;
	protected int agility;
	protected int intelligence;
	protected int range;
	protected int hp, xhp;
	protected int mp, xmp;
	
	public Hero() {
		
	}
	
	public int Attack(Army enemy) {
		int damage = Math.min(this.damage() - enemy.defend(this), 1);
		return damage;
	}

	private int damage() {
		return strength;
	}
	
}
