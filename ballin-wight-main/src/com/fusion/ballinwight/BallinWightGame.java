package com.fusion.ballinwight;

import com.badlogic.gdx.Game;
import com.fusion.ballinwight.models.WorldMap;
import com.fusion.ballinwight.screens.SplashScreen;

public class BallinWightGame extends Game {

	private WorldMap wm;
	
	@Override
	public void create() {
		this.setScreen(new SplashScreen(this));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
